﻿using System;
using System.Collections.Generic;
using Bombs.Configs;
using Bombs.Data;
using Bombs.Interfaces;
using Bombs.Utils;

namespace Bombs
{
	public class Game : IDisposable
	{
		private GameConfig gameConfig = null;
		private BombSpawner bombSpawner = null;
		private CreatureSpawner creatureStawner = null;
		private WallsGenerator wallsGenerator = null;
			
		private List<IBomb> bombs = new List<IBomb>();
		private List<ICreature> creatures = new List<ICreature>();
		
		public Game(GameConfig config)
		{
			gameConfig = config;
			
			wallsGenerator = new WallsGenerator();
			wallsGenerator.Generate(config.WallGeneratorConfig);
			
			bombSpawner = new BombSpawner(gameConfig.BombConfigs, gameConfig.BombSpawnerConfig.Bounds, 
				gameConfig.BombSpawnerConfig.Interval);
			bombSpawner.OnSpawn += OnSpawnBomb;
			
			creatureStawner = new CreatureSpawner(gameConfig.CreatureConfigs, gameConfig.CreatureSpawnerConfig.Bounds,
				gameConfig.CreatureSpawnerConfig.Interval);
			creatureStawner.OnSpawn += OnCreatureSpawn;
		}

		public void DoUpdate(float deltaTime)
		{
			bombSpawner.DoUpdate(deltaTime);
			creatureStawner.DoUpdate(deltaTime);
		}

		public void Dispose()
		{
			bombSpawner.OnSpawn -= OnSpawnBomb;
			creatureStawner.OnSpawn -= OnCreatureSpawn;
			foreach (var bomb in bombs)
			{
				bomb.OnExplode -= OnExplodeBomb;
			}
			foreach (var creature in creatures)
			{
				creature.OnDie -= OnCreatureDie;
			}
			bombSpawner.Dispose();
			creatureStawner.Dispose();
		}

		private void OnCreatureSpawn(ICreature creature)
		{
			creatures.Add(creature);
			creature.OnDie += OnCreatureDie;
		}
		
		private void OnCreatureDie(ICreature creature)
		{
			creature.OnDie -= OnCreatureDie;
			creatures.Remove(creature);
		}

		private void OnSpawnBomb(IBomb bomb)
		{
			bombs.Add(bomb);
			bomb.OnExplode += OnExplodeBomb;
		}

		private void OnExplodeBomb(IBomb bomb)
		{
			if (bomb.BombType != BombType.Cluster && bomb is IDamaging)
			{
				DamageMaker.Process(bomb.Position, bomb.Radius, ((IDamaging)bomb).Damage, creatures);
			}
			bomb.OnExplode -= OnExplodeBomb;
			bombs.Remove(bomb);
		}
	}
}
