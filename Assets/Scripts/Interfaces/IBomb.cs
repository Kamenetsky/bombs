﻿using System;
using Bombs.Configs;

namespace Bombs.Interfaces
{
	public interface IBomb : IExploding<IBomb>
	{		
		BombType BombType { get; }
	}
}
