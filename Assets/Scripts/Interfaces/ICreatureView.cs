﻿using UnityEngine;

namespace Bombs.Interfaces
{
	public interface ICreatureView
	{
		Vector3 Position { get; }
		float Radius { get; }
		void Destroy();
	}
}
