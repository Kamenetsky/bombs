﻿using UnityEngine;

namespace Bombs.Interfaces
{
	public interface ICreature : IMortal<ICreature>
	{
		Vector3 Position { get; }
		float Radius { get; }
	}
}
