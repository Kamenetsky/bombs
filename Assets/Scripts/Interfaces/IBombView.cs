﻿using System;
using UnityEngine;

namespace Bombs.Interfaces
{
	public interface IBombView
	{
		event Action<IBombView> OnImpact;
		
		Vector3 Position { get; }
		void Destroy();
		void ShowEffect(float radius);
	}
}
