﻿using System;

namespace Bombs.Interfaces
{
	public interface IMortal<T>
	{
		event Action<T> OnDie;
		
		float Health { get; set; }
		void Die();
	}
}
