﻿using System;
using UnityEngine;

namespace Bombs.Interfaces
{	
	public interface ISpawner<T, V> where T : class where V : class
	{
		event Action<T> OnSpawn;
		
		void Spawn(V config, Vector3 position);
		void DoUpdate(float deltaTime);
	}
}
