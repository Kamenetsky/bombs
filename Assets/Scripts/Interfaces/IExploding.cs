﻿using System;
using UnityEngine;

namespace Bombs.Interfaces
{
	public interface IExploding<T>
	{
		event Action<T> OnExplode;
		
		float Radius { get; }
		Vector3 Position { get; set; }
		void Explode();
	}
}
