﻿namespace Bombs.Interfaces
{
	public interface IEffect
	{
		void Show(float radius);
	}
}
