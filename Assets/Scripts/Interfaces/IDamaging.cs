﻿namespace Bombs.Interfaces
{
	public interface IDamaging
	{
		float Damage { get; }
	}
}
