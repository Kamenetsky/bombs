﻿using Bombs.Configs;
using UnityEngine;

namespace Bombs
{
	public class EntryPoint : MonoBehaviour
	{
		[SerializeField] private GameConfig gameConfig = null;
		
		private Game game;
		
		private void Awake()
	    {
	        game = new Game(gameConfig);
	    }

		private void Update()
		{
			game.DoUpdate(Time.deltaTime);
		}

		private void OnDestroy()
		{
			game.Dispose();
		}
	}
}
