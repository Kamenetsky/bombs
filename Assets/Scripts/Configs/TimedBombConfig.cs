﻿using UnityEngine;

namespace Bombs.Configs
{
	[CreateAssetMenu(fileName = "TimedBombConfig", menuName = "DB/Bombs/TimedBombConfig")]
	public class TimedBombConfig : DamagingBombConfig
	{
		[SerializeField] protected int timer = 0;

		public override BombType BombType => BombType.Timed;
		public int Timer => timer;
	}
}