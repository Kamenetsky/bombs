﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Bombs.Configs
{	
	[System.Serializable]
	public class SpawnerConfig
	{
		[SerializeField] private float interval = 0;
		[SerializeField] private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);

		public virtual float Interval => interval;
		public Bounds Bounds => bounds;
	}

	[System.Serializable]
	public class WallGeneratorConfig
	{
		[SerializeField] private int wallsCount = 0;
		[SerializeField] private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);
		[SerializeField] private List<GameObject> wallPrefabs = new List<GameObject>();
		
		public int WallsCount => wallsCount;
		public Bounds Bounds => bounds;
		public GameObject RandomWallPrefab => wallPrefabs[Random.Range(0, wallPrefabs.Count)];
	}
	
	[CreateAssetMenu(fileName = "GameConfig", menuName = "DB/GameConfig")]
	public class GameConfig : ScriptableObject
	{		
		[SerializeField] private List<BombConfig> bombConfigs = new List<BombConfig>();
		[SerializeField] private List<CreatureConfig> creatureConfigs = new List<CreatureConfig>();
		[SerializeField] private SpawnerConfig bombSpawnerConfig = null;
		[SerializeField] private SpawnerConfig creatureSpawnerConfig = null;
		[SerializeField] private WallGeneratorConfig wallGeneratorConfig = null;

		public List<BombConfig> BombConfigs => bombConfigs;
		public List<CreatureConfig> CreatureConfigs => creatureConfigs;
		public SpawnerConfig BombSpawnerConfig => bombSpawnerConfig;
		public SpawnerConfig CreatureSpawnerConfig => creatureSpawnerConfig;
		public WallGeneratorConfig WallGeneratorConfig => wallGeneratorConfig;
	}
}
