﻿using UnityEngine;

namespace Bombs.Configs
{
	[System.Serializable]
	public class CreatureConfig
	{
		[SerializeField] private float health = 0f;
		[SerializeField] private GameObject prefab = null;

		public float Health => health;
		public GameObject Prefab => prefab;
	}
}
