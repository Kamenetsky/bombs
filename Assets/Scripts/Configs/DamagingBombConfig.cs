﻿using UnityEngine;

namespace Bombs.Configs
{
	[CreateAssetMenu(fileName = "DamagingBombConfig", menuName = "DB/Bombs/DamagingBombConfig")]
	public class DamagingBombConfig : BombConfig
	{
		[SerializeField] protected float damage = 0f;

		public override BombType BombType => BombType.Damaged;
		public float Damage => damage;
	}
}