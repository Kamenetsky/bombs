﻿using UnityEngine;

namespace Bombs.Configs
{
	public enum BombType
	{
		Default = 0,
		Damaged = 10,
		Cluster = 20,		
		Timed = 30,
	}
	
	public class BombConfig : ScriptableObject
	{
		[SerializeField] protected float radius = 0f;
		[SerializeField] protected GameObject prefab = null;

		public virtual BombType BombType => BombType.Default;
		public float Radius => radius;
		public GameObject Prefab => prefab;
	}
}
