﻿using UnityEngine;

namespace Bombs.Configs
{
	[CreateAssetMenu(fileName = "ClusterBombConfig", menuName = "DB/Bombs/ClusterBombConfig")]
	public class ClusterBombConfig : BombConfig
	{
		[SerializeField] protected int clustersCount = 0;
		[SerializeField] protected BombConfig childBombConfig = null;

		public override BombType BombType => BombType.Cluster;
		public int ClustersCount => clustersCount;
		public BombConfig ChildBombConfig => childBombConfig;
	}
}