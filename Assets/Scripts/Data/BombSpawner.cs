﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bombs.Configs;
using Bombs.Interfaces;
using Bombs.Views;
using UnityEngine;

namespace Bombs.Data
{
	public class BombSpawner : Spawner<IBomb, BombConfig>, IDisposable
	{
		private readonly Dictionary<BombType, Type> bombByType = new Dictionary<BombType, Type>
		{
			{ BombType.Damaged, typeof(SimpleBomb) },
			{ BombType.Cluster, typeof(ClusterBomb) },
			{ BombType.Timed, typeof(TimedBomb) },
		};
		
		private readonly Dictionary<BombType, Type> bombViewByType = new Dictionary<BombType, Type>
		{
			{ BombType.Damaged, typeof(BombView) },
			{ BombType.Cluster, typeof(BombView) },
			{ BombType.Timed, typeof(BombView) },
		};
		
		private Dictionary<IBomb, IBombView> bombs = new Dictionary<IBomb, IBombView>();
		
		public BombSpawner(IList<BombConfig> configs, Bounds bounds, float interval = -1f, Transform parent = null) : 
			base(configs, bounds, interval, parent)
		{		
		}
		
		public override void Spawn(BombConfig config, Vector3 position)
		{
			if (!bombByType.ContainsKey(config.BombType))
				throw new Exception(string.Format("No bomb type specified for {0}. ", config.BombType));
			
			if (!bombViewByType.ContainsKey(config.BombType))
				throw new Exception(string.Format("No bomb view type specified for {0}. ", config.BombType));

			Type bombType = bombByType[config.BombType]; 
			var bomb = (IBomb)Activator.CreateInstance(bombType, new object[2]{ config, this });
			bomb.OnExplode += Destroy;
			var go = GameObject.Instantiate(config.Prefab);
			go.transform.position = position;
			go.transform.SetParent(parent);
			var view = (IBombView)go.AddComponent(bombViewByType[config.BombType]);
			view.OnImpact += OnBombImpact;
			bombs.Add(bomb, view);
			BroadcastSpawned(bomb);
		}

		public override void DoUpdate(float deltaTime)
		{
			base.DoUpdate(deltaTime);
			foreach (var bomb in bombs)
			{
				bomb.Key.Position = bomb.Value.Position;
			}
		}

		public void Dispose()
		{
			foreach (var bomb in bombs)
			{
				bomb.Key.OnExplode -= Destroy;
				if (bomb.Value != null)
					bomb.Value.OnImpact -= OnBombImpact;
			}
		}

		private void Destroy(IBomb bomb)
		{
			bomb.OnExplode -= Destroy;
			if (!bombs.ContainsKey(bomb))
				return;

			IBombView view = bombs[bomb];
			if (view != null)
			{
				view.OnImpact -= OnBombImpact;
				view.ShowEffect(bomb.Radius);
				view.Destroy();
				view = null;
			}

			bombs.Remove(bomb);
		}

		private void OnBombImpact(IBombView view)
		{			
			IBomb bomb = bombs.First(x => x.Value == view).Key;
			bomb.Explode();
		}
	}
}
