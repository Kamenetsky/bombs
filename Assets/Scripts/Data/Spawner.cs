﻿using System;
using System.Collections.Generic;
using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Data
{
	public abstract class Spawner<T, V> : ISpawner<T, V> where T : class where V : class
	{
		public event Action<T> OnSpawn;
		
		private readonly IList<V> configs;
		
		protected Transform parent = null;
		
		private Bounds bounds = new Bounds(Vector3.zero, Vector3.one);
		private float interval = 0f;
		private float time = 0f;
		private bool isLooped = false;

		protected Spawner(IList<V> configs, Bounds bounds, float interval = -1f, Transform parent = null)
		{
			this.configs = configs;
			this.parent = parent;
			this.bounds = bounds;
			this.interval = interval;
			time = 0f;
			isLooped = interval > float.Epsilon;
		}

		public virtual void DoUpdate(float deltaTime)
		{
			if (!isLooped)
				return;
			
			time += deltaTime;
			if (time >= interval)
			{
				time = 0f;
				SpawnRandom();
			}
		}

		private void SpawnRandom()
		{
			V config = configs[UnityEngine.Random.Range(0, configs.Count)];
			Spawn(config, Vector3Ext.RandomInBounds(bounds));			
		}

		protected void BroadcastSpawned(T item)
		{
			OnSpawn?.Invoke(item);
		}

		public abstract void Spawn(V config, Vector3 position);	
	}
}
