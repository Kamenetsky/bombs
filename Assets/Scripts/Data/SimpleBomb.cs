﻿using Bombs.Configs;
using Bombs.Interfaces;

namespace Bombs.Data
{
	public class SimpleBomb : Bomb<DamagingBombConfig>, IDamaging
	{
		public float Damage => damage;
		
		private float damage = 0f;

		public SimpleBomb(DamagingBombConfig config, BombSpawner spawner) : base(config)
		{
			damage = config.Damage;
		}
	}
}
