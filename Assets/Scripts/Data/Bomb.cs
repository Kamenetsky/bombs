﻿using System;
using Bombs.Configs;
using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Data
{
	public abstract class Bomb<T> : IBomb where T: BombConfig
	{
		public event Action<IBomb> OnExplode;

		public float Radius => radius;
		public BombType BombType => bombType;
		public Vector3 Position { get; set; }

		private float radius = 0f;
		private BombType bombType;

		protected Bomb(T config)
		{
			radius = config.Radius;
			bombType = config.BombType;
		}
		
		public virtual void Explode()
		{
			OnExplode?.Invoke(this);
		}
	}
}
