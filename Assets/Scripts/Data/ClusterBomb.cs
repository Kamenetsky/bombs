﻿using System.Collections.Generic;
using Bombs.Configs;
using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Data
{
	public class ClusterBomb : Bomb<ClusterBombConfig>
	{	
		private readonly int clustersCount = 0;
		private readonly BombConfig childBombConfig = null;

		private BombSpawner spawner = null;
		
		public ClusterBomb(ClusterBombConfig config, BombSpawner spawner) : base(config)
		{
			clustersCount = config.ClustersCount;
			childBombConfig = config.ChildBombConfig;
			this.spawner = spawner;
		}

		public override void Explode()
		{
			if (childBombConfig != null)
			{
				for (int i = 0; i < clustersCount; ++i)
				{
					spawner.Spawn(childBombConfig, Position);
				}
			}

			base.Explode();
		}
	}
}
