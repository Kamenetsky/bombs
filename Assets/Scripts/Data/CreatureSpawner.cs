﻿using System;
using System.Collections.Generic;
using Bombs.Configs;
using Bombs.Interfaces;
using Bombs.Views;
using UnityEngine;

namespace Bombs.Data
{
	public class CreatureSpawner : Spawner<ICreature, CreatureConfig>, IDisposable
	{
		private Dictionary<ICreature, ICreatureView> creatures = new Dictionary<ICreature, ICreatureView>();
		
		public CreatureSpawner(IList<CreatureConfig> configs, Bounds bounds, float interval = -1, Transform parent = null) 
			: base(configs, bounds, interval, parent)
		{
		}

		public override void Spawn(CreatureConfig config, Vector3 position)
		{
			var go = GameObject.Instantiate(config.Prefab);
			go.transform.position = position;
			go.transform.SetParent(parent);
			var view = (ICreatureView) go.AddComponent<CreatureView>();
			var creature = new Creature(config.Health, view.Position, view.Radius);
			creature.OnDie += Destroy;
			creatures.Add(creature, view);
			BroadcastSpawned(creature);
		}

		public void Dispose()
		{
			foreach (var creature in creatures)
			{
				creature.Key.OnDie -= Destroy;
			}
		}

		private void Destroy(ICreature creature)
		{
			creature.OnDie -= Destroy;
			if (!creatures.ContainsKey(creature))
				return;

			if (creatures[creature] != null)
			{
				creatures[creature].Destroy();
				creatures[creature] = null;
			}
			
			creatures.Remove(creature);
		}
	}
}
