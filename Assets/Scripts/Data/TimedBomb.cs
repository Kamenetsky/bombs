﻿using System.Threading.Tasks;
using Bombs.Configs;
using UnityEngine;

namespace Bombs.Data
{
	public class TimedBomb : SimpleBomb
	{
		private readonly int timer = 0;
		
		public TimedBomb(TimedBombConfig config, BombSpawner spawner) : base(config, spawner)
		{
			timer = config.Timer;
		}

		public override async void Explode()
		{
			await TimedExplode();
		}

		private async Task TimedExplode()
		{
			await Task.Delay(timer);
			base.Explode();
		}
	}
}
