﻿using System;
using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Data
{
	public class Creature : ICreature
	{
		public event Action<ICreature> OnDie;

		public Vector3 Position => position;
		public float Radius => radius;

		private Vector3 position;
		private float radius;

		public Creature(float health, Vector3 position, float radius)
		{
			Health = health;
			this.position = position;
			this.radius = radius;
		}

		public float Health
		{
			get { return health; }
			set
			{
				if (isDead)
					return;
				
				health = value;
				if (health <= 0f)
				{
					health = 0f;
					Die();
				}
			}
		}

		private float health = 0f;
		private bool isDead = false;

		public virtual void Die()
		{
			isDead = true;
			OnDie?.Invoke(this);
		}
	}
}
