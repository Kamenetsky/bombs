﻿using Bombs.Interfaces;
using UnityEngine;
using DG.Tweening;

namespace Bombs.Views
{
	public class BlowEffect : MonoBehaviour, IEffect
	{
		[SerializeField] private float time = 0.1f;
		
		private void Start()
	    {
	        transform.localScale = Vector3.zero;
	    }

		public void Show(float radius)
		{
			transform.SetParent(null);
			transform.DOScale(radius, time).OnComplete(() => Destroy(gameObject));
		}
	}
}
