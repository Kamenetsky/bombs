﻿using System;
using System.Collections;
using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Views
{
	public class BombView : MonoBehaviour, IBombView
	{ 		
		public event Action<IBombView> OnImpact;

		public Vector3 Position => transform.position;

		private IEffect effect = null;

		private void Awake()
		{
			effect = GetComponentInChildren<IEffect>();
		}

		public void ShowEffect(float radius)
		{
			if (effect != null)
			{
				effect.Show(radius);
				effect = null;
			}
		}
		
		public void Destroy()
		{
			if (gameObject != null)
				Destroy(gameObject);
		}

		private void OnTriggerEnter(Collider other)
		{
			OnImpact?.Invoke(this);
		}
	}
}
