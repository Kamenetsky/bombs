﻿using Bombs.Interfaces;
using UnityEngine;

namespace Bombs.Views
{
	[RequireComponent(typeof(MeshRenderer))]
	public class CreatureView : MonoBehaviour, ICreatureView
	{
		public Vector3 Position => transform.position;
		public float Radius => meshRenderer.bounds.size.sqrMagnitude;

		private MeshRenderer meshRenderer = null;

		private void Awake()
		{
			meshRenderer = GetComponent<MeshRenderer>();
		}

		public void Destroy()
		{
			if (gameObject != null)
				Destroy(gameObject);
		}
	}
}
