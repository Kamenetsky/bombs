﻿using System.Collections.Generic;
using Bombs.Configs;
using UnityEngine;

namespace Bombs.Utils
{
	public class WallsGenerator
	{
	    private List<GameObject> walls = new List<GameObject>();
		
		public void Generate(WallGeneratorConfig config, Transform parent = null)
		{
			Clear();
			for (int i = 0; i < config.WallsCount; ++i)
			{
				var wall = GameConfig.Instantiate(config.RandomWallPrefab);
				walls.Add(wall);
				wall.transform.position = Vector3Ext.RandomInBounds(config.Bounds);
				wall.transform.SetParent(parent);
			}
		}

		public void Clear()
		{
			foreach (var wall in walls)
			{
				GameObject.Destroy(wall);
			}
			walls.Clear();
		}
	}
}
