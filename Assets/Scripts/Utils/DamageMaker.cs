﻿using System.Collections.Generic;
using System.Numerics;
using Bombs.Interfaces;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Bombs.Utils
{
	public static class DamageMaker 
	{
	    public static void Process(Vector3 position, float radius, float damage, List<ICreature> creatures)
	    {
		    int layerMask = LayerMask.GetMask("Wall");
		    for (int i = 0; i < creatures.Count; ++i)
		    {
			    Vector3 direction = creatures[i].Position - position;
			    if (direction.sqrMagnitude + creatures[i].Radius > radius * radius)
				    continue;

			    if (Physics.Raycast(position, direction.normalized, radius, layerMask))
				    continue;

			    creatures[i].Health -= damage;
		    }
	    }
	}
}
