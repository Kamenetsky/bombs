﻿using UnityEngine;

public static class Vector3Ext
{
	public static Vector3 RandomInBounds(Bounds bounds)
	{
		var xPos = Random.Range(bounds.min.x, bounds.max.x);
		var yPos = Random.Range(bounds.min.y, bounds.max.y);
		var zPos = Random.Range(bounds.min.z, bounds.max.z);

		return new Vector3(xPos, yPos, zPos);
	}
}
